package com.beast.wicall.store;

import java.util.List;

public interface PeerStoreInterface {
    public void store(String ip, String uname);

    public String get(String uname);

    List<String> getList();
}
