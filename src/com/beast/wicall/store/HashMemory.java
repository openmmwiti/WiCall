package com.beast.wicall.store;

import com.beast.wicall.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class HashMemory implements PeerStoreInterface {
    private static final String TAG = "HashMemoryStore";
    HashMap<String, String> hash = new HashMap<String, String>();

    public HashMemory() {

    }
    @Override
    public List<String> getList() {

        ArrayList<String> list = new ArrayList<String>();
        Iterator<String> iterator = hash.keySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }
    @Override
    public void store(String ip, String uname) {
        hash.put(uname, ip);
        Log.i(TAG, "Updated/Added member ={" + uname + "/" + ip + "} StoreCount=" + hash.size());
    }

    @Override
    public String get(String uname) {
        return hash.get(uname);
    }
}
