package com.beast.wicall;

import com.beast.wicall.discovery.Client;
import com.beast.wicall.discovery.Server;
import com.beast.wicall.store.HashMemory;
import com.beast.wicall.store.PeerStoreInterface;
import com.beast.wicall.utils.Helper;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        Log.setOutPutStream(System.out);
        Log.setLoggingLevel(Log.DEBUG);
        Log.debug("Application", "Starting application services");

        PeerStoreInterface ps = new HashMemory();

        Settings.setPeerStore(ps);
        Server.run();
        Client.run();
        com.beast.wicall.call.Server.run();
        com.beast.wicall.call.Client callService = new com.beast.wicall.call.Client();
        Thread.sleep(3000);
        //Log.setLoggingLevel(Log.INFO);
        //callService.call("Gri4");

        Scanner sc = new Scanner(System.in);
        System.out.println("------ Application Menu ------ Beta1");
        System.out.println("1. Make Call");
        switch (sc.nextInt()) {
            case 1:
                System.out.println("Select user:");
                int x = 0;
                for (String s : Settings.getPeerStore().getList()) {
                    x++;
                    System.out.println("\t" + x + ": " + s);
                }
                String username = Settings.getPeerStore().getList().get(sc.nextInt() - 1);
                Log.d("Application", "Attempting to make new call to: " + username);
                callService.call(username);
                //Log.setLoggingLevel(Log.INFO);

        }

    }
}
