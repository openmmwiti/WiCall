package com.beast.wicall;

import java.io.IOException;
import java.io.OutputStream;

public class Log {

    public static final int VERBOSE = 0;
    public static final int INFO = 1;
    public static final int DEBUG = 2;
    public static final int ERROR = 3;

    private static OutputStream out;
    private static int loggingLevel;

    public static void d(String tag, String error) {
        if (loggingLevel <= DEBUG) {
            print("D:\\" + tag + ":" + error);
        }
    }

    public static void i(String tag, String error) {
        if (loggingLevel <= INFO) {
            print("I:\\" + tag + ":" + error);
        }
    }

    public static void v(String tag, String error) {
        if (loggingLevel <= VERBOSE) {
            print("V:\\" + tag + ":" + error);
        }
    }

    public static void e(String tag, String error) {
        if (loggingLevel <= ERROR) {
            print("E:\\" + tag + ":" + error);
        }
    }

    private static void print(String s) {
        s = s.concat("\n");
        try {
            out.write(s.getBytes());
        } catch (IOException e) {
            System.out.println("IO error occured while logging");
            //e.printStackTrace();
        }
    }

    public static void debug(String tag, String error) {
        Log.d(tag, error);
    }

    public static void info(String tag, String error) {
        Log.i(tag, error);
    }

    public static void verbose(String tag, String error) {
        Log.v(tag, error);
    }

    public static void error(String tag, String error) {
        Log.e(tag, error);
    }

    public static void setOutPutStream(OutputStream out) {
        Log.out = out;
    }

    public static void setLoggingLevel(int loggingLevel) {
        Log.loggingLevel = loggingLevel;
    }
}
