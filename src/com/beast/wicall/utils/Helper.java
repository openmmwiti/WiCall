package com.beast.wicall.utils;

import com.beast.wicall.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

public class Helper {
    protected static final String TAG = "Helper";
    protected static HashMap<String, String> settings;

    public static InetAddress getBroadCastAddressFromAddresses(List<InterfaceAddress> addresses) {
        for (InterfaceAddress ia : addresses) {
            if (ia.getBroadcast() != null) {
                return ia.getBroadcast();
            }
        }
        return null;
    }

    public static String byteArrayToString(byte array[]) {

        String s = "";
        for (byte b : array) {
            s += b + ",";
        }
        return s;
    }

    public static HashMap<String, String> getSettings() {
        if (settings == null) {
            settings = new HashMap<String, String>();
            File s = new File("settings.conf");
            try {
                Scanner sc = new Scanner(s);
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    String key = line.substring(0, line.indexOf('='));
                    String value = line.substring(key.length() + 1);
                    settings.put(key, value);
                }
            } catch (Exception e) {
                Log.e(TAG, "Couldnt load settings. Exiting!");
                e.printStackTrace();
                System.exit(1);
            }
        }
        return settings;
    }

    public static ArrayList<NetworkInterface> getBroadcastAddress() {
        Log.info(TAG, "Querying available interfaces");
        ArrayList<NetworkInterface> nis = new ArrayList<NetworkInterface>();
        Enumeration<NetworkInterface> networkInterfaceEnumeration;
        try {
            networkInterfaceEnumeration = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaceEnumeration.hasMoreElements()) {
                NetworkInterface ni = networkInterfaceEnumeration.nextElement();
                if (ni.supportsMulticast()) {
                    InetAddress address = getBroadCastAddressFromAddresses(ni.getInterfaceAddresses());
                    String ipaddress = "Not Available";

                    if (address != null) {
                        ipaddress = "" + address.getHostAddress();
                        nis.add(ni);
                    }

                    Log.v(TAG, "Interface found. Name=" + ni.getDisplayName() + " BroadcastAdress=" + ipaddress + " Multicast=" + ni.supportsMulticast());
                }
            }

        } catch (SocketException e) {
            e.printStackTrace();
        }
        return nis;
    }

    public static byte[] joinArray(byte[] a1, byte[] a2) {
        byte a[] = new byte[a1.length + a2.length];


        for (int x = 0; x < a1.length; x++) {
            a[x] = a1[x];
        }

        for (int x = a2.length - 1; x >= 0; x--) {
            a[x + a1.length] = a2[x];
        }
        return a;
    }

    public static byte[] trimArray(byte[] data, int length) {
        byte b[] = new byte[length];
        for (int x = 0; x < length; x++) {
            b[x] = data[x];
        }
        return b;
    }

}
