package com.beast.wicall.utils;

/**
 * Created by griffins on 9/22/15.
 */
public interface Future {
    public void onFinish(Object o);
}
