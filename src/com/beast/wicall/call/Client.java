package com.beast.wicall.call;

import com.beast.wicall.Log;
import com.beast.wicall.Settings;
import com.beast.wicall.exceptions.InvalidAddressExeption;
import com.beast.wicall.utils.Helper;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    private static final String TAG = "CallClient";
    private boolean onCall;

    public Client() {

    }

    public void cancelCall() {
        onCall = false;
    }

    public void call(String username) {

        int rnd = 0;
        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }

        String ip = Settings.getPeerStore().get(username);

        if (ip == null) {
            Log.e(TAG, "User '" + username + "' not found. Call aborted");
            return;
        }

        try {
            DatagramSocket socket = new DatagramSocket(rnd);

            socket.setBroadcast(true);

            InetAddress broadcast = InetAddress.getByName(ip);
            if (broadcast == null) {
                Log.d(TAG, "Invalid Broadcast address");
                throw (new InvalidAddressExeption());
            }

            // Send the call request

            DatagramPacket packet;
            packet = new DatagramPacket(Settings.getCallSignature(),
                    Settings.getCallSignature().length, broadcast,
                    Settings.getCallPort());
            socket.send(packet);
            Log.d(TAG, "Call request sent to: " + broadcast.getHostAddress() + ":" + Settings.getCallPort());

            // + " DataLength=" + Settings.getSignature().length + ",
            // Data={" + Helper.byteArrayToString(Settings.getSignature()) + "}");

            byte receiveBuffer[] = new byte[1024];
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

            // blocking call take care.we do it only for 30 seconds.
            socket.receive(receivePacket);

            this.processResponse(receivePacket);
            socket.close();
        } catch (Exception e) {
            Log.e(TAG, "Client Socket error - " + e);
            return;
            //  e.printStackTrace();
        }

    }

    private int checkReceived(byte[] recieveBuffer) {


        byte[] buffer2 = Helper.joinArray(Settings.getCallReplySignature(), Settings.getCallReadySignature());


        for (int x = 0; x < buffer2.length; x++) {

            if (recieveBuffer.length < x) {
                return 0;
            }

            if (buffer2[x] != recieveBuffer[x]) {
                return 0;
            }
        }

        return Integer.valueOf(new String(recieveBuffer).substring(buffer2.length));
    }

    private void processResponse(final DatagramPacket receivePacket) {
        byte buffer[] = Helper.trimArray(receivePacket.getData(), receivePacket.getLength());
        final int port = checkReceived(buffer);
        if (port >= 6024) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    makeCall(receivePacket.getAddress().getHostAddress(), port);
                }
            });

            executor = Executors.newSingleThreadExecutor();
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    listenCall(port, receivePacket.getAddress().getHostAddress());
                }
            });
        } else {
            Log.d(TAG, "Call Rejected");
        }
    }

    private void listenCall(int port, String ip) {
        try {
            port = port + 1;
            ServerSocket listener = new ServerSocket(port);

            Log.d(TAG, "Waiting for call link.Listening on port: " + port);
            Socket socket = listener.accept();
            Log.d(TAG, "Link established call in progress");
            byte[] receiveBuffer = new byte[8000];
            byte buffer[];

            Audio audio = new Audio();
            onCall = true;
            while (onCall) {
                int count = socket.getInputStream().read(receiveBuffer);
                buffer = Helper.trimArray(receiveBuffer, count);
                Log.i(TAG, "Got some audio DataLenght={" + buffer.length + "}");
                audio.put(buffer);
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void makeCall(String ip, int port) {
        int rnd = 0;
        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }

        try {

            Thread.sleep(100);
            if (ip == null) {
                Log.d(TAG, "Invalid ip address");
                throw (new InvalidAddressExeption());
            }

            Socket socket = new Socket(ip, port);

            // Send the call request
            Audio audio = new Audio();
            onCall = true;
            while (onCall) {
                byte buffer[] = audio.get();
                OutputStream out = socket.getOutputStream();
                out.write(buffer);
                //Log.d(TAG, "Sending call data to: " + ip + ":" + port);

                // + " DataLength=" + Settings.getSignature().length + ",
                // Data={" + Helper.byteArrayToString(Settings.getSignature()) + "}");
            }
            Log.debug(TAG, "Call ended");
        } catch (Exception e) {
            Log.d(TAG, "OutBound Call Socket error: " + e);

        }

    }
}
