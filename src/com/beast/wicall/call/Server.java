package com.beast.wicall.call;


import com.beast.wicall.Log;
import com.beast.wicall.Settings;
import com.beast.wicall.exceptions.InvalidAddressExeption;
import com.beast.wicall.utils.Helper;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final String TAG = "CallServer";

    private DatagramPacket callPacket;
    private boolean onCall;

    public Server() {

    }

    public static void run() {
        new Server().execute();
    }

    private void execute() {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(new Runnable() {
            @Override
            public void run() {
                listen();
            }
        });
    }

    private void listen() {
        try {

            DatagramSocket socket = new DatagramSocket(Settings.getCallPort());

            byte[] receiveBuffer = new byte[1024];
            byte buffer[];

            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

            Log.d(TAG, "Call server started.");

            while (true) {
                Log.i(TAG, "Blocking for receive");

                socket.receive(receivePacket);
                buffer = Helper.trimArray(receivePacket.getData(), receivePacket.getLength());
                if (Server.this.checkCallSignature(buffer)) {
                    Log.d(TAG, "Someone is calling us");
                    Server.this.sendCallReply(socket, receivePacket);
                } else if (Server.this.checkReply(buffer)) {

                    String s = new String(buffer).substring(Settings.getReplySignature().length);

                    Log.info(TAG, "Response Signature=" + s + " IP=" + receivePacket.getAddress().getHostAddress());

                } else {
                    Log.e(TAG, "Invalid incoming command. From=" + receivePacket.getAddress().getHostAddress() + " Raw=" + Helper.byteArrayToString(buffer));
                }
            }

            //socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean checkReply(byte[] recieveBuffer) {

        for (int x = 0; x < Settings.getReplySignature().length; x++) {
            if (recieveBuffer.length < x) {
                return false;
            }
            if (Settings.getReplySignature()[x] != recieveBuffer[x]) {
                return false;
            }
        }
        return true;
    }

    private void sendCallReply(DatagramSocket socket, final DatagramPacket packet) {
        int rnd = 0;
        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }


        try {
            // Send the broadcast package
            byte[] buffer = Helper.joinArray(Settings.getCallReplySignature(), Settings.getCallReply());

            final InetAddress broadcast = packet.getAddress();

            callPacket = new DatagramPacket(buffer, buffer.length, broadcast, packet.getPort());
            socket.send(callPacket);
            if (buffer[Settings.getCallReplySignature().length] != Settings.getCallReadySignature()[0]) {
                Log.d(TAG, "Call rejected");
                return;
            }

            Log.d(TAG, "Call from: " + broadcast.getHostAddress() + ":" + packet.getPort() + " accepted");// + " DataLength=" + buffer.length + " Data={" + Helper.byteArrayToString(buffer) + "} String={" + new String(buffer) + "}");

            ExecutorService executor = Executors.newSingleThreadExecutor();

            executor.submit(new Runnable() {
                @Override
                public void run() {
                    listenCall(Settings.getCurrentCallPort(), broadcast.getHostAddress());
                }
            });
            executor = Executors.newSingleThreadExecutor();

            executor.submit(new Runnable() {
                @Override
                public void run() {
                    makeCall(broadcast.getHostAddress(), Settings.getCurrentCallPort());
                }
            });
        } catch (Exception e) {
            Log.d(TAG, "Client Socket error - " + e.getMessage());
            return;
            //  e.printStackTrace();
        }
    }

    private void listenCall(int port, String ip) {
        try {

            ServerSocket listener = new ServerSocket(port);

            Log.d(TAG, "Waiting for call link.Listening on port: " + port);
            Socket socket = listener.accept();
            Log.d(TAG, "Link established call in progress");
            byte[] receiveBuffer = new byte[8000];
            byte buffer[];

            Audio audio = new Audio();
            onCall = true;
            while (onCall) {
                int count = socket.getInputStream().read(receiveBuffer);
                buffer = Helper.trimArray(receiveBuffer, count);
                Log.i(TAG, "Got some audio DataLenght={" + buffer.length + "}");
                audio.put(buffer);
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void makeCall(String ip, int port) {

        port = port + 1;
        int rnd = 0;

        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }

        try {

            Thread.sleep(100);
            if (ip == null) {
                Log.d(TAG, "Invalid ip address");
                throw (new InvalidAddressExeption());
            }

            Socket socket = new Socket(ip, port);
            // Send the call request
            Audio audio = new Audio();
            onCall = true;
            while (onCall) {
                byte buffer[] = audio.get();
                OutputStream out = socket.getOutputStream();
                out.write(buffer);
                Log.i(TAG, "Sending call data to: " + ip + ":" + port);

                // + " DataLength=" + Settings.getSignature().length + ",
                // Data={" + Helper.byteArrayToString(Settings.getSignature()) + "}");
            }
            Log.debug(TAG, "Call ended");
        } catch (Exception e) {
            Log.d(TAG, "OutBound Call Socket error: " + e);

        }

    }

    private boolean checkCallSignature(byte[] recieveBuffer) {
        for (int x = 0; x < Settings.getCallSignature().length; x++) {
            if (recieveBuffer.length < x) {
                return false;
            }
            if (Settings.getCallSignature()[x] != recieveBuffer[x]) {
                return false;
            }
        }
        return true;
    }
}