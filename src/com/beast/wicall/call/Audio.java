package com.beast.wicall.call;

import com.beast.wicall.Log;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by griffins on 9/25/15.
 */
public class Audio {
    private static final String TAG = "Audio";
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    float sampleRate = 8000;
    int sampleSizeInBits = 8;
    int channels = 1;
    boolean signed = true;
    boolean bigEndian = true;

    TargetDataLine line;
    AudioFormat format;
    SourceDataLine line1 = null;

    public Audio() {
        format = new AudioFormat(sampleRate, sampleSizeInBits, channels, signed,
                bigEndian);

        try {
            line = (TargetDataLine) AudioSystem.getLine(new DataLine.Info(TargetDataLine.class, format));

            line.open(format);
            line.start();
            line1 = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, format));


        } catch (Exception e) {
            Log.e(TAG, "Audio error-" + e);
        }
    }

    public void put(byte[] audio) {

        InputStream input = new ByteArrayInputStream(audio);


        final AudioInputStream ais = new AudioInputStream(input, format, audio.length
                / format.getFrameSize());
        int bufferSize = (int) format.getSampleRate() * format.getFrameSize();

        byte buffer[] = new byte[bufferSize];

        try {

            line1.start();
            line1.open(format);

            int count;
            while ((count = ais.read(buffer, 0, buffer.length)) != -1) {
                if (count > 0) {
                    line1.write(buffer, 0, count);
                }
            }
            //line1.drain();
            //line1.close();
        } catch (Exception e) {
            System.err.println("I/O or other problems: " + e);
            System.exit(-3);
        }
    }

    public byte[] get() {

        byte buffer[] = new byte[line.available()];
        int count = line.read(buffer, 0, buffer.length);

       // Log.d(TAG, "Available " + line.available() + " , caught=" + buffer.length);
        //Log.d(TAG, Helper.byteArrayToString(buffer));
        // buffer = Helper.trimArray(buffer, count);
        return buffer;

    }
}