package com.beast.wicall.exceptions;


public class InvalidAddressExeption extends Exception {
    public InvalidAddressExeption() {
        super();
    }

    @Override
    public String getMessage() {
        return " Invalid broadcast address";
    }
}
