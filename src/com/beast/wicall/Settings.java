package com.beast.wicall;

import com.beast.wicall.store.PeerStoreInterface;
import com.beast.wicall.utils.Helper;

import java.util.Scanner;

public class Settings {
    final static byte signature[] = {4, 32, 17, 53};
    final static byte replySignature[] = {4, 32, 18, 54};
    private static byte[] callSignature = {4, 32, 19, 55};
    private static byte[] callReplySignature = {4, 32, 20, 56};
    private static byte[] readyCallReply = {0};
    private static byte[] rejectCallReply = {1};

    private static int port = 4041;
    private static int callPort = 4046;
    private static PeerStoreInterface peerStore;
    private static int currentCallPort;


    public static byte[] getSignature() {
        return signature;
    }

    public static byte[] getReplySignature() {
        return replySignature;
    }

    public static int getPort() {
        return port;
    }

    public static int getCallPort() {
        return callPort;
    }

    public static String getName() {
        String name = "Unknown";
        name = Helper.getSettings().get("USERNAME");
        return name;
    }

    public static void setPeerStore(PeerStoreInterface peerStore) {
        Settings.peerStore = peerStore;
    }

    public static PeerStoreInterface getPeerStore() {
        return peerStore;
    }

    public static byte[] getCallSignature() {
        return callSignature;
    }

    public static byte[] getCallReplySignature() {
        return callReplySignature;
    }

    public static byte[] getCallReadySignature() {
        return readyCallReply;
    }

    public static int getCurrentCallPort() {
        return currentCallPort;
    }

    public static byte[] getCallReply() {
        int rnd = 0;
        while (rnd < 6024) {
            rnd = (int) (Math.random() * 64000);
        }
        currentCallPort = rnd;
        System.out.println("Hello we have an incoming call!\n\t 1. Accept \n\t 2. Reject");
        int reply = new Scanner(System.in).nextInt();
        return Helper.joinArray(reply == 1 ? readyCallReply : rejectCallReply, (rnd + "").getBytes());
    }
}
