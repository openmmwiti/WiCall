package com.beast.wicall.discovery;


import com.beast.wicall.utils.Helper;
import com.beast.wicall.exceptions.InvalidAddressExeption;
import com.beast.wicall.Log;
import com.beast.wicall.Settings;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final String TAG = "BroadCastServer";

    private DatagramPacket identityPacket;

    public Server() {

    }

    public static void run() {
        new Server().execute();
    }

    private void execute() {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(new Runnable() {
            @Override
            public void run() {
                listen();
            }
        });
    }

    private void listen() {
        try {
            DatagramPacket packet;

            DatagramSocket socket = new DatagramSocket(Settings.getPort());

            byte[] recieveBuffer = new byte[1024];
            byte buffer[];

            DatagramPacket recievePacket = new DatagramPacket(recieveBuffer, recieveBuffer.length);

            while (true) {
                socket.receive(recievePacket);
                buffer = Helper.trimArray(recievePacket.getData(), recievePacket.getLength());
                if (Server.this.checkSignature(buffer)) {
                    Log.i(TAG, "Someone asking us to identify");
                    Server.this.sendReply();
                } else if (Server.this.checkReply(buffer)) {

                    String s = new String(buffer).substring(Settings.getReplySignature().length);

                    Log.info(TAG, "Response Signature=" + s + " IP=" + recievePacket.getAddress().getHostAddress());

                    Settings.getPeerStore().store(recievePacket.getAddress().getHostAddress(), s);

                } else {
                    Log.d(TAG, "Invalid incoming command. Raw=" + Helper.byteArrayToString(buffer));
                }
            }

            //socket.close();

        } catch (Exception e) {
            Log.e(TAG, "Error: " + e);
        }
    }

    private boolean checkReply(byte[] recieveBuffer) {

        for (int x = 0; x < Settings.getReplySignature().length; x++) {
            if (recieveBuffer.length < x) {
                return false;
            }
            if (Settings.getReplySignature()[x] != recieveBuffer[x]) {
                return false;
            }
        }
        return true;
    }

    private void sendReply() {
        int rnd = 0;
        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }

        ArrayList<NetworkInterface> interfaces = Helper.getBroadcastAddress();
        for (NetworkInterface ni : interfaces) {

            try {
                DatagramSocket socket = new DatagramSocket(rnd);

                socket.setBroadcast(true);

                InetAddress broadcast = Helper.getBroadCastAddressFromAddresses(ni.getInterfaceAddresses());

                if (broadcast == null) {
                    Log.d(TAG, "Invalid Broadcast address");
                    throw (new InvalidAddressExeption());
                }

                // Send the broadcast package
                byte[] buffer = Helper.joinArray(Settings.getReplySignature(), Settings.getName().getBytes());


                identityPacket = new DatagramPacket(buffer, buffer.length, broadcast, Settings.getPort());

                socket.send(identityPacket);
                socket.close();

                Log.i(TAG, "Discovery reply Sent to: " + broadcast.getHostAddress());// + " DataLength=" + buffer.length + " Data={" + Helper.byteArrayToString(buffer) + "} String={" + new String(buffer) + "}");
            } catch (Exception e) {
                Log.e(TAG, "Client Socket error - " + e.getMessage());
                return;
                //  e.printStackTrace();
            }
        }

    }


    private boolean checkSignature(byte[] recieveBuffer) {
        for (int x = 0; x < Settings.getSignature().length; x++) {
            if (recieveBuffer.length < x) {
                return false;
            }
            if (Settings.getSignature()[x] != recieveBuffer[x]) {
                return false;
            }
        }
        return true;
    }
}