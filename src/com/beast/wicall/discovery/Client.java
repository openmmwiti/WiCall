package com.beast.wicall.discovery;

import com.beast.wicall.utils.Helper;
import com.beast.wicall.exceptions.InvalidAddressExeption;
import com.beast.wicall.Log;
import com.beast.wicall.Settings;

import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    private static final String TAG = "BroadCastClient";
    int timeout = 20000;

    public void check() throws InvalidAddressExeption {

        int rnd = 0;
        while (rnd < 1024) {
            rnd = (int) (Math.random() * 64000);
        }

        ArrayList<NetworkInterface> interfaces = Helper.getBroadcastAddress();
        for (NetworkInterface ni : interfaces) {

            try {
                DatagramSocket socket = new DatagramSocket(rnd);

                socket.setBroadcast(true);

                InetAddress broadcast = Helper.getBroadCastAddressFromAddresses(ni.getInterfaceAddresses());

                if (broadcast == null) {
                    Log.d(TAG, "Invalid Broadcast address");
                    throw (new InvalidAddressExeption());
                }

                // Send the broadcast package

                DatagramPacket packet;
                packet = new DatagramPacket(Settings.getSignature(), Settings.getSignature().length, broadcast, Settings.getPort());
                socket.send(packet);
                Log.i(TAG, "Discovery request sent to: " + broadcast.getHostAddress() + ":" + Settings.getPort());// + " DataLength=" + Settings.getSignature().length + ",Data={" + Helper.byteArrayToString(Settings.getSignature()) + "}");
                socket.close();
            } catch (Exception e) {
                Log.e(TAG, "Client Socket error - " + e.getMessage());
                return;
                //  e.printStackTrace();
            }
        }

    }

    public static void run() {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(new Runnable() {
            @Override
            public void run() {

                Client c = new Client();

                while (true) {
                    try {
                        c.check();
                        Thread.sleep(c.timeout);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Client Thread caught interrupt exception! Exiting");
                        break;
                    } catch (InvalidAddressExeption invalidAddressExeption) {
                        Log.e(TAG, "Client Thread caught exception! " + invalidAddressExeption.getMessage());
                        // break;
                    }
                }
            }
        });

    }

}
